from django.urls import path
from .views import welcome

app_name= 'Welcome'
urlpatterns = [
    path('', welcome, name='welcome')
]
