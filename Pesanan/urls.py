from django.urls import path
from .views import buatPesanan, updatePesanan, daftarPesanan, hitungPesanan, daftarkanPesanan, deletePesanan

app_name = 'Pesanan'
urlpatterns = [
	path('buat', buatPesanan, name='buatPesanan'),
	path('update', updatePesanan, name='updatePesanan'),
	path('', daftarPesanan, name='daftarPesanan'),
	path('hitung', hitungPesanan, name='hitungPesanan'),
	path('mendaftar', daftarkanPesanan, name='daftarkanPesanan'),
	path('delete', deletePesanan, name='deletePesanan'),
]