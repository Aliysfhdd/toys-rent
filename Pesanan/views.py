from django.shortcuts import render, redirect
from django.db import connection
from django.http import JsonResponse

import os
import psycopg2
import datetime

response = {}
now = datetime.datetime.now()
def hitungPesanan(request):
	if request.session['role']!='':
		if request.session['role']=='admin' and request.POST:
			biayaSewa = 0
			jumlahHari = float(request.POST['lamaSewa'])
			with connection.cursor() as cursor:
				cursor.execute('''SET SEARCH_PATH TO TOYS_RENT ''')
				for x in request.POST.getlist('barang[]'):
					query = '''SELECT harga_sewa FROM INFO_BARANG_LEVEL, ANGGOTA WHERE id_barang=%s AND no_ktp=%s AND nama_level = level;'''
					cursor.execute(query, (x, request.POST['idAnggota']))					
					harga = cursor.fetchone()[0]					
					biayaSewa += jumlahHari * harga					
			
			data = {'jumlah':biayaSewa, 'id_pemesan':request.POST['idAnggota'], 'waktu':now}
			return JsonResponse(data)
		return redirect('Pesanan:daftarPesanan')
	else:
		return redirect('Welcome:welcome')

def daftarkanPesanan(request):
	if request.POST and request.session['role']=='admin':
		jumlahHari = float(request.POST['lamaSewa'])	
		with connection.cursor() as cursor:
			cursor.execute('''SET SEARCH_PATH TO TOYS_RENT ''')
			for x in request.POST.getlist('barang[]'):
				cursor.execute('''SELECT id_pemesanan FROM PEMESANAN ORDER BY CAST(id_pemesanan AS INT) DESC LIMIT 1;''')
				id_pesanan = int(cursor.fetchone()[0])+1

				query = '''SELECT harga_sewa FROM INFO_BARANG_LEVEL, ANGGOTA WHERE id_barang=%s AND no_ktp=%s AND nama_level = level;'''
				cursor.execute(query, (x, request.POST['id_pemesan']))
				harga = jumlahHari * cursor.fetchone()[0]

				query = '''INSERT INTO PEMESANAN VALUES(%s, %s, 1, %s, 1000, %s, 'DIPROSES')'''
				cursor.execute(query, (id_pesanan, now, harga, request.POST['id_pemesan']))

				cursor.execute('''SELECT no_urut FROM BARANG_PESANAN ORDER BY CAST(no_urut AS INT) DESC LIMIT 1;''')
				no_urut = int(cursor.fetchone()[0])+1

				query = '''INSERT INTO BARANG_PESANAN VALUES(%s, %s, %s, %s, %s, NULL, 'DIPROSES')'''
				cursor.execute(query, (id_pesanan, no_urut, x, now, jumlahHari))

				connection.commit()
				print(harga)
				print(id_pesanan)
				print(no_urut)
		return JsonResponse({})
	elif request.POST and request.session['role']=='anggota':
		return JsonResponse({})
	else:
		return redirect('Welcome:welcome')

def buatPesanan(request):
	if request.session['role']!='':

		with connection.cursor() as cursor:
			cursor.execute('''SET SEARCH_PATH TO TOYS_RENT ''')
			query = '''SELECT nama_lengkap, P.no_ktp FROM ANGGOTA A, PENGGUNA P
			WHERE A.no_ktp = P.no_ktp ORDER BY nama_lengkap ASC;'''
			cursor.execute(query)
			rows = cursor.fetchall()
			response['anggota'] = []
			for row in rows:
				response['anggota'].append(row)

			query = '''SELECT nama_item, id_barang FROM BARANG ORDER BY nama_item;'''
			cursor.execute(query)
			rows = cursor.fetchall()
			response['barang'] = []
			for row in rows:
				response['barang'].append(row)

		return render(request, 'buat-pesanan.html', response)
	else:
		return redirect('Welcome:welcome')

def updatePesanan(request):
	if request.session['role']!='':
		id_barang = request.GET['id_barang']
		id_pesanan = request.GET['id_pesanan']
		status = request.GET['status']

		with connection.cursor() as cursor:			
			cursor.execute('''SET SEARCH_PATH TO TOYS_RENT ''')
			query = '''SELECT lama_sewa FROM BARANG_PESANAN WHERE id_pemesanan=%s;'''
			cursor.execute(query, (id_pesanan,))
			lama_sewa = cursor.fetchone()[0]
			response['lama_sewa'] = lama_sewa
			response['status_terkini'] = status
			response['id_barang'] = id_barang

			query = '''SELECT nama FROM STATUS;'''
			cursor.execute(query)
			rows = cursor.fetchall()
			response['status'] = []
			for row in rows:
				response['status'].append(row)

			query = '''SELECT nama_item, id_barang FROM BARANG ORDER BY nama_item;'''
			cursor.execute(query)
			rows = cursor.fetchall()
			response['barang'] = []
			for row in rows:
				response['barang'].append(row)

		return render(request, 'update-pesanan.html', response)
	else:
		return redirect('Welcome:welcome')

def daftarPesanan(request):
	if request.session['role']!='':
		with connection.cursor() as cursor:
			cursor.execute('''SET SEARCH_PATH TO TOYS_RENT ''')
			if request.session['role']=='admin':
				query = '''SELECT P.id_pemesanan, B.nama_item, P.harga_sewa, P.status, B.id_barang
						FROM BARANG B, PEMESANAN P, BARANG_PESANAN BP
						WHERE BP.id_pemesanan = P.id_pemesanan AND BP.id_barang = B.id_barang
						ORDER BY id_pemesanan ASC;'''
				cursor.execute(query)
				rows = cursor.fetchall()
				response['pesanan'] = []
				for row in rows:
					response['pesanan'].append(row)
			else:
				no_ktp_pemesan = request.session['no_ktp']
				query = '''SELECT P.id_pemesanan, B.nama_item, P.harga_sewa, P.status, B.id_barang
						FROM BARANG B, PEMESANAN P, BARANG_PESANAN BP
						WHERE BP.id_pemesanan = P.id_pemesanan AND BP.id_barang = B.id_barang AND no_ktp_pemesan = %s
						ORDER BY id_pemesanan ASC;'''
				cursor.execute(query, (no_ktp_pemesan,))
				rows = cursor.fetchall()
				response['pesanan'] = []
				for row in rows:
					response['pesanan'].append(row)
		return render(request, 'daftar-pesanan.html', response)
	else:
		return redirect('Welcome:welcome')

def deletePesanan(request):
	if request.session['role']!='':
		with connection.cursor() as cursor:
			cursor.execute('''SET SEARCH_PATH TO TOYS_RENT ''')
			id_pemesanan = request.GET['idPemesanan']
			query = '''DELETE FROM PEMESANAN
					WHERE id_pemesanan=%s;'''
			cursor.execute(query, (id_pemesanan,))
			connection.commit()
		return redirect('Pesanan:daftarPesanan')
	else:
		return redirect('Welcome:welcome')