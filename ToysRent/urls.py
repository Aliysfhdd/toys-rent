"""ToysRent URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('Welcome.urls', namespace='Welcome')),
    path('chat/', include('Chat.urls', namespace='Chat')),
    path('barang/', include('Barang.urls', namespace='Barang')),
    path('auth/', include('Auth.urls', namespace='Auth')),
    path('item/', include('Item.urls', namespace='Item')),
    path('pesanan/', include('Pesanan.urls', namespace='Pesanan')),
    path('level/', include('Level.urls', namespace='Level')),
    path('profile/', include('Profile.urls', namespace='Profile')),
]
