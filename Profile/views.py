from django.shortcuts import render
from django.db import connection
response={}
def viewprofile(request):
	no_ktp = request.session['no_ktp']
	queries = "select * from toys_rent.pengguna where toys_rent.pengguna.no_ktp='" + no_ktp + "';"
	with connection.cursor() as cursor:
        
		cursor.execute(queries)
		data = cursor.fetchall()[0] 

	nama = data[1]
	email = data[2]
	tanggal_lahir = data[3]
	nomor_telepon = data[4]

	response['nama'] = nama
	response['email'] = email
	response['tanggal_lahir'] = tanggal_lahir
	response['nomor_telepon'] = nomor_telepon

	if request.session['role'] == 'anggota':
		queries = "select * from toys_rent.anggota where toys_rent.anggota.no_ktp='" + no_ktp + "';"
		with connection.cursor() as cursor:
			
			cursor.execute(queries)
			data = cursor.fetchall()[0]

		poin = data[1]
		level = data[2]

		response['poin'] = poin
		response['level'] = level

	return render(request, 'viewprofile.html', response)
