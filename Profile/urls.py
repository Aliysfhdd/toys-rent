from django.urls import path
from .views import viewprofile

app_name= 'Profile'
urlpatterns = [
    path('', viewprofile, name='viewprofile'),
    
]
