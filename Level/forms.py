from django import forms

class LevelForm(forms.Form):
	levelName = forms.CharField(max_length=20, required=True, label='Nama Level',
		widget=forms.TextInput(attrs={'placeholder': 'Masukkan nama level...'}))
	minPoin = forms.IntegerField(required=True, label='Minimum Poin',
		widget=forms.NumberInput(attrs={'placeholder': 'Masukkan minimum poin...', 'min':0}))
	desc = forms.CharField(label='Deskripsi',
		widget=forms.TextInput(attrs={'placeholder': 'Masukkan deskripsi...'}))