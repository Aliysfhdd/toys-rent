from django.urls import path
from .views import buatLevel, updateLevel, daftarLevel, deleteLevel

app_name = 'Level'
urlpatterns = [
	path('buat', buatLevel, name='buatLevel'),
	path('update', updateLevel, name='updateLevel'),
	path('', daftarLevel, name='daftarLevel'),
	path('delete', deleteLevel, name='deleteLevel'),
]