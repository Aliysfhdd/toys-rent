from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import LevelForm
from django.db import connection

import os
import psycopg2

response = {}
def buatLevel(request):
	if request.session['role']=='admin':
		if request.method == 'POST':		
			levelName = request.POST['levelName']
			minPoin = float(request.POST['minPoin'])
			desc = request.POST['desc']
			
			with connection.cursor() as cursor:
				cursor.execute('''SET SEARCH_PATH TO TOYS_RENT ''')
				query = '''INSERT INTO LEVEL_KEANGGOTAAN VALUES(%s, %s, %s);'''
				cursor.execute(query, (levelName, minPoin, desc))
				connection.commit()
			return redirect('/level')

		response['form'] = LevelForm
		return render(request, 'buat-level.html', response)
	else:
		return redirect('Welcome:welcome')

def updateLevel(request):
	if request.session['role']=='admin':
		if request.method == 'POST':
			with connection.cursor() as cursor:
				cursor.execute('''SET SEARCH_PATH TO TOYS_RENT ''')
				levelName = request.POST['init_name'] #default level name
				if request.POST['levelName'] != request.POST['init_name']:
					query = '''UPDATE LEVEL_KEANGGOTAAN SET nama_level=%s WHERE nama_level=%s;'''
					cursor.execute(query, (request.POST['levelName'], levelName))
					levelName =  request.POST['levelName'] #new level name
				if request.POST['minPoin'] != request.POST['init_poin']:
					query = '''UPDATE LEVEL_KEANGGOTAAN SET minimum_poin=%s WHERE nama_level=%s;'''
					cursor.execute(query, (request.POST['minPoin'], levelName))
				if request.POST['desc'] != request.POST['init_desc']:
					query = '''UPDATE LEVEL_KEANGGOTAAN SET deskripsi=%s WHERE nama_level=%s;'''
					cursor.execute(query, (request.POST['desc'], levelName))
				connection.commit()
			return redirect('/level')

		data = {'levelName':request.GET['level'],
		'minPoin':request.GET['poin'],
		'desc':request.GET['desc']
		}
		form = LevelForm(initial=data)

		response = {'form':form,
		'initial_name':request.GET['level'],
		'initial_poin':request.GET['poin'],
		'initial_desc':request.GET['desc']
		}
		return render(request, 'update-level.html', response)
	else:
		return redirect('Welcome:welcome')

def daftarLevel(request):
	if request.session['role']=='admin':
		with connection.cursor() as cursor:
			cursor.execute('''SET SEARCH_PATH TO TOYS_RENT ''')
			query = '''SELECT * FROM LEVEL_KEANGGOTAAN ORDER BY minimum_poin;'''
			cursor.execute(query)
			rows = cursor.fetchall()
			
			response['level'] = []
			for row in rows:
				response['level'].append(row)

		return render(request, 'daftar-level.html', response)
	else:
		return redirect('Welcome:welcome')

def deleteLevel(request):
	if request.session['role']=='admin':
		with connection.cursor() as cursor:
			cursor.execute('''SET SEARCH_PATH TO TOYS_RENT ''')
			levelName = request.GET['level']
			query = '''DELETE FROM LEVEL_KEANGGOTAAN 
					WHERE nama_level=%s;'''
			cursor.execute(query, (levelName,))
			connection.commit()
		return redirect('/level')
	else:
		return redirect('Welcome:welcome')
