from django.shortcuts import render, redirect
from .forms import barangForm
from django.db import  connection

response={}
def barang(request):
    if request.session['role']!='':
        create_table_query = '''Select * from toys_rent.barang; '''
        with connection.cursor() as cursor:
            cursor.execute(create_table_query)
            rows = cursor.fetchall()
            response['barang']=[]
            for row in rows:
                response['barang'].append(row)
        category= '''select nama from toys_rent.kategori'''
        with connection.cursor() as cursor:
            cursor.execute(category)
            kategori= cursor.fetchall()
            response['list_kategori']= []
            for row in kategori:
                response['list_kategori'].append(row[0])
        return render(request, 'barang.html', response)
    else:
        return redirect('Welcome:welcome')

def barang_sort_nama(request):
    if request.session['role']!='':
        
        create_table_query = '''Select * from toys_rent.barang
                                order by toys_rent.barang.nama_item
                                ; '''
        with connection.cursor() as cursor:
            cursor.execute(create_table_query)
            rows = cursor.fetchall()
            print ("\nRows: \n")
            response['barang']=[]
            for row in rows:
                response['barang'].append(row)
        return render(request, 'barang.html', response)
    else:
        return redirect('Welcome:welcome')

def barang_sort_id(request):
    if request.session['role']!='':
        
        create_table_query = '''Select * from toys_rent.barang
                                order by toys_rent.barang.id_barang
                                ;'''
        with connection.cursor() as cursor:
        
            cursor.execute(create_table_query)
            rows = cursor.fetchall()
            print ("\nRows: \n")
            response['barang']=[]
            for row in rows:
                response['barang'].append(row)
          
        return render(request, 'barang.html', response)
    else:
        return redirect('Welcome:welcome')
def barang_filter(request, kategori=None):
    if request.session['role']!='':
        
        kategori="'"+kategori+"'"
        create_table_query = '''Select b.* from toys_rent.barang b, toys_rent.kategori_item k
                                where nama_kategori='''+ kategori +''' and b.nama_item= k.nama_item
                                ; '''
        with connection.cursor() as cursor:
        
            cursor.execute(create_table_query)
            rows = cursor.fetchall()
            print ("\nRows: \n")
            response['barang']=[]
            for row in rows:
                response['barang'].append(row)
        
        return render(request, 'barang.html', response)
    else:
        return redirect('Welcome:welcome')
def detail(request, barang=None):
    if request.session['role']!='':
        
        create_table_query = '''Select nama, lama_penggunaan, warna, deskripsi, bahan, url_foto from toys_rent.barang b ,toys_rent.item i where  nama=b.nama_item and id_barang= '''+ "'"+ barang +"'"+'''; '''
        with connection.cursor() as cursor:
        
            cursor.execute(create_table_query)
            rows = cursor.fetchall()
            print ("\nRows: \n")
            response['barang']=rows[0]

        list_kategori= '''select distinct nama_kategori from toys_rent.kategori_item k, toys_rent.barang b, toys_rent.item i where nama=k.nama_item and b.nama_item=k.nama_item and id_barang= '''+ "'"+ barang +"'"+'''; '''
        with connection.cursor() as cursor:
            cursor.execute(list_kategori)
            kategori = cursor.fetchall()
            response['barang_kategori']=[]
            for i in kategori:
                response['barang_kategori'].append(i[0])
            print(kategori)
        list_review= '''select review from toys_rent.barang_dikirim d where d.id_barang= '''+ "'"+ barang +"'"+ ''';'''
        with connection.cursor() as cursor:
            cursor.execute(list_review)
            list_review = cursor.fetchall()
            response['review']=[]
            print(list_review)
            for i in list_review:
                response['review'].append(i[0])
           
        return render(request, 'detail.html', response)
    else:
        return redirect('Welcome:welcome')
def add(request):
    if request.session['role']=='admin':
        form = barangForm(request.POST or None)
        response['addForm'] = form
        return render(request, 'add.html', response)
    else:
        return redirect('Welcome:welcome')
def addSubmit(request):
    if request.session['role']=='admin':
        
        form= barangForm(request.POST or None)
        if request.method == 'POST' and form.is_valid():
            id = "'"+request.POST['id']+"'"
            nama = "'"+request.POST['nama']+"'"
            warna = "'"+request.POST['warna']+"'"
            url = "'"+request.POST['url']+"'"
            kondisi = "'"+request.POST['kondisi']+ "'"
            penggunaan = request.POST['penggunaan']
            pemilik = "'"+ request.POST['pemilik']+ "'"
            #Query level
            get_level=''' select nama_level from toys_rent.level_keanggotaan;'''
            with connection.cursor() as cursor:
       
                cursor.execute(get_level)
                level = cursor.fetchall()
                list_level= []
                for rows in level:
                    list_level.append(rows[0])
                kategori_dict={}
                for i in list_level:
                    kategori_dict[i]={
                        i: request.POST[i],
                        "Sewa"+i:request.POST["harga sewa "+i]
                    }
                
                print(kategori_dict)
            get_pemilik = '''select no_ktp from toys_rent.pengguna p where p.nama_lengkap='''+pemilik +''';'''
            with connection.cursor() as cursor:
                cursor.execute(get_pemilik)
                ktp = "'"+ cursor.fetchall()[0][0]+"'"
                print(ktp)     
            query= '''insert into toys_rent.barang values('''+id+''','''+ nama+''','''+warna+''','''+url+''','''+kondisi+''','''+penggunaan+''','''+ktp +'''); '''
            with connection.cursor() as cursor:
                cursor.execute(query)
            with connection.cursor() as cursor:
                for i in kategori_dict:
                    print(i)
                    print(kategori_dict[i])
                    print(kategori_dict[i][i])
                    print(kategori_dict[i]["Sewa"+i])
                    cursor.execute('''insert into toys_rent.info_barang_level values('''+id+''','''+"'"+i+"'"+''', '''+"'"+kategori_dict[i]["Sewa"+i]+"'"+''','''+"'"+kategori_dict[i][i]+"'"+ ''') ''')
                    print(i)
                    print(kategori_dict[i])  
            connection.commit()
            return redirect('Barang:barang')
        else:
            return redirect('Barang:add',)
    else:
        return redirect('Welcome:welcome')
def delete(request,barang=None):
    if request.session['role']=='admin':
        
        print(barang)
        barang= "'"+ barang + "'"
        del_barang = '''delete from toys_rent.barang b where b.id_barang= '''+barang +''';'''
        with connection.cursor() as cursor:       
            cursor.execute(del_barang)
        connection.commit()
        return redirect('Barang:barang')
    else:
        return redirect('Welcome:welcome')

def update(request, barang=None):
    if request.session['role']=='admin':
    
        barang= "'"+ barang + "'"
        get_barang = '''select * from toys_rent.barang b where b.id_barang= '''+barang +''';'''
        with connection.cursor() as cursor: 
            cursor.execute(get_barang)
            barangs=cursor.fetchall()[0]    
        ktp= "'"+barangs[6]+"'"
        get_pemilik = '''select nama_lengkap from toys_rent.pengguna p where p.no_ktp='''+ktp +''';'''
        with connection.cursor() as cursor:       
            cursor.execute(get_pemilik)
            pemilik=cursor.fetchall()[0]
        get_level='''select nama_level,harga_sewa,porsi_royalti from toys_rent.info_barang_level i, toys_rent.barang b where b.id_barang=i.id_barang and b.id_barang='''+barang+''';'''
        with connection.cursor() as cursor:
            cursor.execute(get_level)
            level=cursor.fetchall()
        lis={
            "id":barangs[0],
            "nama":barangs[1],
            "warna":barangs[2],
            "url":barangs[3],
            "kondisi":barangs[4],
            "penggunaan":barangs[5],
            "pemilik":pemilik,    
        }
        for i in level:
                lis[i[0]]= i[2]
                lis["harga sewa "+i[0]]=i[1]
        
        form = barangForm(request.POST or None, initial=lis)
        response['updateForm'] = form
        return render(request, 'update.html', response)
    else:
        return redirect('Welcome:welcome')
def updateSubmit(request):
    if request.session['role']=='admin':
        form= barangForm(request.POST or None)
        if request.method == 'POST' and form.is_valid():
            id = "'"+request.POST['id']+"'"
            nama = "'"+request.POST['nama']+"'"
            warna = "'"+request.POST['warna']+"'"
            url = "'"+request.POST['url']+"'"
            kondisi = "'"+request.POST['kondisi']+ "'"
            penggunaan = request.POST['penggunaan']
            pemilik = "'"+ request.POST['pemilik']+ "'"
            #Query level
            get_level=''' select nama_level from toys_rent.level_keanggotaan;'''
            with connection.cursor() as cursor:
                cursor.execute(get_level)
                level = cursor.fetchall()
            list_level= []
            for rows in level:
                list_level.append(rows[0])
            kategori_dict={}
            for i in list_level:
                kategori_dict[i]={
                    i: request.POST[i],
                    "Sewa"+i:request.POST["harga sewa "+i]
                }
            
            print(kategori_dict)
            print(pemilik)
            get_pemilik = '''select no_ktp from toys_rent.pengguna p where p.nama_lengkap='''+pemilik +''';'''
            with connection.cursor() as cursor: 
                cursor.execute(get_pemilik)
                ktp = "'"+ cursor.fetchall()[0][0]+"'"
            print(ktp)
            
            query= '''update toys_rent.barang set id_barang='''+id+''',nama_item='''+ nama+''',warna='''+warna+''',url_foto='''+url+''',kondisi='''+kondisi+''',lama_penggunaan='''+penggunaan+''',no_ktp_penyewa='''+ktp +''' where id_barang='''+id+'''; '''
            with connection.cursor() as cursor:
                cursor.execute(query)
            
            with connection.cursor() as cursor: 
                for i in kategori_dict:
                    print(i)
                    print(kategori_dict[i])
                    print(kategori_dict[i][i])
                    print(kategori_dict[i]["Sewa"+i])
                    cursor.execute('''update toys_rent.info_barang_level set id_barang='''+id+''',nama_level='''+"'"+i+"'"+''',harga_sewa= '''+"'"+kategori_dict[i]["Sewa"+i]+"'"+''',porsi_royalti='''+"'"+kategori_dict[i][i]+"'"+''' where id_barang='''+id+'''and nama_level='''+"'"+i+"'" )
                    print(i)
                    print(kategori_dict[i]) 
            connection.commit()
            return redirect('Barang:barang')
        else:
            return redirect('Barang:add',)
    else:
        return redirect('Welcome:welcome')
# Create your views here.

