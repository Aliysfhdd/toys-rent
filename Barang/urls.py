from django.urls import path
from .views import barang, detail, add, update, barang_sort_nama, addSubmit, updateSubmit, barang_sort_id, barang_filter, delete

app_name= 'Barang'
urlpatterns = [
    path('', barang, name='barang'),
    path('sort/name', barang_sort_nama, name='barangsortnama'),
    path('sort/id', barang_sort_id, name='barangsortid'),
    path('add/', add, name='add'),
    path('addSubmit/', addSubmit, name='addSubmit'),
    path('detail/<barang>/', detail, name='detail'),
    path('delete/<barang>/', delete, name='delete'),
    path('update/<barang>/', update, name='update'),
    path('updateSubmit/', updateSubmit, name='updateSubmit'),
    path('filter/<kategori>', barang_filter, name='filter'),
    
]
