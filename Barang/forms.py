from django import forms
from django.db import connection

#QUERY ITEM
get_item = '''Select nama from toys_rent.item; '''
with connection.cursor() as cursor:
    cursor.execute(get_item)
    rows = cursor.fetchall()
item= []
for row in rows:
    item.append((row[0], row[0]))
item= tuple(item)

#QUERY PEMILIK
get_pemilik = '''select nama_lengkap from toys_rent.pengguna p, toys_rent.anggota a where a.no_ktp=p.no_ktp;'''
with connection.cursor() as cursor:    
    cursor.execute(get_pemilik)
    row = cursor.fetchall()
pemilik= []
for rows in row:
    pemilik.append((rows[0], rows[0]))
pemilik= tuple(pemilik)

#Query level
get_level=''' select nama_level from toys_rent.level_keanggotaan;'''
with connection.cursor() as cursor: 
    cursor.execute(get_level)
    level = cursor.fetchall()
list_level= []
for rows in level:
    list_level.append(rows[0])

class barangForm(forms.Form):
    id = forms.CharField(label = "Id Barang", required = True, max_length=60, widget=forms.TextInput())
    nama = forms.ChoiceField(label="Nama Item", required = True,choices=item)
    warna = forms.CharField(label = "Warna", required = False, max_length=60, widget=forms.TextInput())
    url = forms.CharField(label = "Url Foto", required = False, max_length=60, widget=forms.TextInput())
    kondisi = forms.CharField(label = "Kondisi", required = True, max_length=60, widget=forms.TextInput())
    penggunaan = forms.CharField(label = "Lama Penggunaan", required = False, widget=forms.NumberInput())
    pemilik = forms.ChoiceField(label="Pemilik/Penyewa", required = True,choices=pemilik)
    def __init__(self, *args, **kwargs):
        super(barangForm, self).__init__(*args, **kwargs)
        for level in list_level:
            royalti_field_name = level
            harga_sewa_field_name = "harga sewa %s" % (level)
            self.fields[royalti_field_name] = forms.IntegerField(label="Persen Royalti")
            self.fields[harga_sewa_field_name] = forms.IntegerField(label="Harga Sewa")
