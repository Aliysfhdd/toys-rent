from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import LoginForm, AdminRegForm, UserRegForm
from django.db import connection
# import os
# import psycopg2
# try:
# 	connection = psycopg2.connect(
# 		dbname=os.environ['dbname'],
# 		user= os.environ['user'],
# 		password=os.environ['password'],
# 		host= os.environ['host'],
# 		port= os.environ['port']
# 		)
# 	cursor = connection.cursor()
# 	# Print PostgreSQL Connection properties
# 	print ( connection.get_dsn_parameters(),"\n")
# 	# Print PostgreSQL version
# 	cursor.execute("SELECT version();")
# 	record = cursor.fetchone()
# 	print("You are connected to - ", record,"\n")
# except (Exception, psycopg2.Error) as error :
# 	print ("Error while connecting to PostgreSQL", error)

response={}
def authscreen(request):
	if request.method == 'POST':
		form = LoginForm(request.POST)
		if form.is_valid():
			no_ktp = form.cleaned_data['no_ktp']
			email = form.cleaned_data['email']
			anggota = False
			admin = False

			# QUERY CEK EMAIL+KTP ANGGOTA
			queries = "select * from toys_rent.pengguna natural join toys_rent.anggota"
			with connection.cursor() as cursor:
        
				cursor.execute(queries)
				rows = cursor.fetchall()
			for row in rows:
				if (no_ktp== row[0]) and (email == row[2]):
					anggota = True
					break
				else:
					continue

			# QUERY CEK EMAIL+KTP ADMIN
			queries = "select * from toys_rent.admin natural join toys_rent.pengguna"
			with connection.cursor() as cursor:
				cursor.execute(queries)
				rows = cursor.fetchall()
			for row in rows:
				if (no_ktp == row[0]) and (email == row[2]):
					admin = True
					break
				else:
					continue

			if admin:
				print("Admin login")
				queries = "select * from toys_rent.admin natural join toys_rent.pengguna where no_ktp='" + no_ktp + "'"
				with connection.cursor() as cursor:
					cursor.execute(queries)
					rows = cursor.fetchall()
				nama = ""
				for row in rows:
					nama = row[1]

				request.session['nama'] = nama
				request.session['no_ktp'] = no_ktp
				request.session['email'] = email
				request.session['role'] = 'admin'

				return HttpResponseRedirect('/pesanan/')
			elif anggota:
				print("Anggota login")
				queries = "select * from toys_rent.pengguna natural join toys_rent.anggota"
				with connection.cursor() as cursor:
					cursor.execute(queries)
					rows = cursor.fetchall()
				nama = ""
				for row in rows:
					nama = row[1]

				request.session['nama'] = nama
				request.session['no_ktp'] = no_ktp
				request.session['email'] = email
				request.session['role'] = 'anggota'

				return HttpResponseRedirect('/barang/')
			else:
				print("Wrong credentials!")
				return render(request, 'authscreen_false_credentials.html', response)

		else:	
			form = LoginForm()
			response['form'] = form

	else:	
		form = LoginForm()
		response['form'] = form
	return render(request, 'authscreen.html', response)

def logout(request):
	request.session['nama'] = ""
	request.session['no_ktp'] = ""
	request.session['email'] = ""
	request.session['role'] = ""

	return HttpResponseRedirect('/auth/')

def reguser(request):
	request.session['valid'] = ''

	if request.method == 'POST':
		form = UserRegForm(request.POST)
		if form.is_valid():
			no_ktp = form.cleaned_data['no_ktp']
			nama = form.cleaned_data['nama_lengkap']
			email = form.cleaned_data['email']
			tanggal_lahir = form.cleaned_data['tanggal_lahir']
			nomor_telepon = form.cleaned_data['nomor_telepon']

			nama_alamat_1 = form.cleaned_data['nama_alamat_1']
			jalan_1 = form.cleaned_data['jalan_1']
			nomor_1 = form.cleaned_data['nomor_1']
			kota_1 = form.cleaned_data['kota_1']
			kodepos_1 = form.cleaned_data['kodepos_1']

			# CHECK KTP AND EMAIL AVAILABILITY
			if (not account_valid(no_ktp, email)):
				request.session['valid'] = 'F'
				return render(request, 'reguser.html', response)

			# INSERT TO TABLE PENGGUNA
			queries = "insert into toys_rent.pengguna values ('" + no_ktp + "','" + nama + "','" + email + "','" + tanggal_lahir + "','" + nomor_telepon + "');"
			with connection.cursor() as cursor:        
				cursor.execute(queries)
				connection.commit()

			# INSERT TO TABLE ANGGOTA
			queries = "insert into toys_rent.anggota values ('" + no_ktp + "','0','Bronze');"
			with connection.cursor() as cursor:
			
				cursor.execute(queries)
				connection.commit()

			# INSERT TO TABLE ALAMAT
			queries = "insert into toys_rent.alamat values ('" + no_ktp + "','" + nama_alamat_1 + "','" + jalan_1 + "','" + nomor_1 + "','" + kota_1 + "','" + kodepos_1 + "');"
			with connection.cursor() as cursor:
			
				cursor.execute(queries)
				connection.commit()

			print("VALID!")

			request.session['nama'] = nama
			request.session['no_ktp'] = no_ktp
			request.session['email'] = email
			request.session['role'] = 'anggota'

			return HttpResponseRedirect('/auth/register/success/')


	else:
		form  = UserRegForm()
		response['form'] = form
	return render(request, 'reguser.html', response)

def regadmin(request):
	request.session['valid'] = ''
	
	if request.method == 'POST':
		form = AdminRegForm(request.POST)
		if form.is_valid():
			no_ktp = form.cleaned_data['no_ktp']
			nama = form.cleaned_data['nama_lengkap']
			email = form.cleaned_data['email']
			tanggal_lahir = form.cleaned_data['tanggal_lahir']
			nomor_telepon = form.cleaned_data['nomor_telepon']

			# CHECK KTP AND EMAIL AVAILABILITY
			if (not account_valid(no_ktp, email)):
				request.session['valid'] = 'F'
				return render(request, 'regadmin.html', response)

			# INSERT TO TABLE PENGGUNA
			queries = "insert into toys_rent.pengguna values ('" + no_ktp + "','" + nama + "','" + email + "','" + tanggal_lahir + "','" + nomor_telepon + "');"
			with connection.cursor() as cursor:
			
				cursor.execute(queries)
				connection.commit()

			# INSERT TO TABLE ADMIN
			queries = "insert into toys_rent.admin values ('" + no_ktp + "');"
			with connection.cursor() as cursor:
        
				cursor.execute(queries)
				connection.commit()

			print("VALID!")

			request.session['nama'] = nama
			request.session['no_ktp'] = no_ktp
			request.session['email'] = email
			request.session['role'] = 'admin'
			request.session['valid'] = ''

			return HttpResponseRedirect('/auth/register/success/')
		else:
			form = AdminRegForm()
			response['form'] = form

	else:
		form = AdminRegForm()
		response['form'] = form
	
	return render(request, 'regadmin.html', response)

def regsuccess(request):
	return render(request, 'regsuccess.html')

def chooserole(request):
	return render(request, 'chooserole.html')

def account_valid(no_ktp, email):
	query_ktp = "select * from toys_rent.pengguna where toys_rent.pengguna.no_ktp='" + no_ktp + "';"
	with connection.cursor() as cursor:
        
		cursor.execute(query_ktp);
		data_ktp = cursor.fetchall();

	query_email = "select * from toys_rent.pengguna where toys_rent.pengguna.email='" + email + "';"
	with connection.cursor() as cursor:
        
		cursor.execute(query_email);
		data_email = cursor.fetchall();	

	if len(data_ktp) == 0:
		if len(data_email) == 0:
			return True
		else:
			return False
	else:
		return False
	