from django import forms

class LoginForm(forms.Form):
	no_ktp = forms.CharField(label="Nomor KTP", max_length=30, widget=forms.TextInput(attrs={'placeholder' : 'Masukkan nomor KTP', 'class' : 'form-control'}))
	email = forms.EmailField(label="Email", widget=forms.TextInput(attrs={'placeholder' : 'Masukkan email', 'class' : 'form-control'}))

class UserRegForm(forms.Form):
	no_ktp = forms.CharField(label="Nomor KTP", max_length=30, widget=forms.TextInput(attrs={'placeholder' : 'Masukkan nomor KTP', 'class' : 'form-control'}), required=True)
	nama_lengkap = forms.CharField(label="Nama Lengkap", max_length=30, widget=forms.TextInput(attrs={'placeholder' : 'Masukkan nama lengkap', 'class' : 'form-control'}), required=True)
	email = forms.EmailField(label="Email", widget=forms.TextInput(attrs={'placeholder' : 'Masukkan email', 'class' : 'form-control'}), required=True)
	tanggal_lahir = forms.CharField(label="Tanggal Lahir", widget=forms.TextInput(attrs={'placeholder' : 'yyyy-mm-dd', 'class' : 'form-control'}))
	nomor_telepon = forms.CharField(label="Nomor Telepon", max_length=30, widget=forms.TextInput(attrs={'placeholder' : 'Masukkan nomor telepon', 'class' : 'form-control'}))
	
	nama_alamat_1 = forms.CharField(label="Nama Alamat 1", max_length=30, widget=forms.TextInput(attrs={'placeholder' : 'Masukkan nama alamat 1', 'class' : 'form-control'}), required=True)
	jalan_1 = forms.CharField(label="", max_length=30, widget=forms.TextInput(attrs={'placeholder' : 'Masukkan nama jalan', 'class' : 'form-control'}), required=True)
	nomor_1 = forms.CharField(label="", max_length=30, widget=forms.TextInput(attrs={'placeholder' : 'Masukkan nomor', 'class' : 'form-control'}), required=True)
	kota_1 = forms.CharField(label="", max_length=30, widget=forms.TextInput(attrs={'placeholder' : 'Masukkan kota', 'class' : 'form-control'}), required=True)
	kodepos_1 = forms.CharField(label="", max_length=30, widget=forms.TextInput(attrs={'placeholder' : 'Masukkan kodepos', 'class' : 'form-control'}), required=True)


class AdminRegForm(forms.Form):
	no_ktp = forms.CharField(label="Nomor KTP", max_length=30, widget=forms.TextInput(attrs={'placeholder' : 'Masukkan nomor KTP', 'class' : 'form-control'}), required=True)
	nama_lengkap = forms.CharField(label="Nama Lengkap", max_length=30, widget=forms.TextInput(attrs={'placeholder' : 'Masukkan nama lengkap', 'class' : 'form-control'}), required=True)
	email = forms.EmailField(label="Email", widget=forms.TextInput(attrs={'placeholder' : 'Masukkan email', 'class' : 'form-control'}), required=True)
	tanggal_lahir = forms.CharField(label="Tanggal Lahir", widget=forms.TextInput(attrs={'placeholder' : 'yyyy-mm-dd', 'class' : 'form-control'}))
	nomor_telepon = forms.CharField(label="Nomor Telepon", max_length=30, widget=forms.TextInput(attrs={'placeholder' : 'Masukkan nomor telepon', 'class' : 'form-control'}))