from django.urls import path
from .views import authscreen, reguser, regadmin, regsuccess, chooserole, logout

app_name= 'Auth'
urlpatterns = [
    path('', authscreen, name='authscreen'),
    path('register/user/', reguser, name='reguser'),
    path('register/admin/', regadmin, name='regadmin'),
    path('register/success/', regsuccess, name='regsuccesss'),
    path('logout/', logout, name='logout'),
    path('register/choose-role/', chooserole, name='chooserole'),
    
]
