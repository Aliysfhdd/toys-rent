from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.db import connection
from .forms import AddItemForm

response={}
def displayitems(request, category=None, sortby=None):
    if category != None:
        queries = "select i.nama, k.nama_kategori from toys_rent.item i, toys_rent.kategori_item k where i.nama=k.nama_item and k.nama_kategori='" + category + "';"

    
    elif sortby != None:
        if sortby == 'nama':
            queries = "select toys_rent.item.nama, toys_rent.kategori_item.nama_kategori from toys_rent.item, toys_rent.kategori_item where toys_rent.item.nama=toys_rent.kategori_item.nama_item order by toys_rent.item.nama;"

        elif sortby == 'kategori':
            queries = "select toys_rent.item.nama, toys_rent.kategori_item.nama_kategori from toys_rent.item, toys_rent.kategori_item where toys_rent.item.nama=toys_rent.kategori_item.nama_item order by toys_rent.kategori_item.nama_kategori;"

    else:
        queries = '''select toys_rent.item.nama, toys_rent.kategori_item.nama_kategori from toys_rent.item, toys_rent.kategori_item
        where toys_rent.item.nama=toys_rent.kategori_item.nama_item'''
    
    # QUERY THE ITEMS
    with connection.cursor() as cursor:    
        cursor.execute(queries)
        rows = cursor.fetchall()
    response['item']=[]
    for row in rows:
        response['item'].append((row[0], row[1].replace("_", " ").title()))
        print ("   ", row[0])

    # QUERY AVAILABLE CATEGORIES
    queries = "select * from toys_rent.kategori"
    with connection.cursor() as cursor:
        cursor.execute(queries)
        rows = cursor.fetchall()
    response['categories'] = []
    for row in rows:
        response['categories'].append((row[0], row[0].replace("_", " ").title()))
        print ("   ", row[0])


    
    return render(request, 'displayitems.html', response)

def deleteitem(request, name):
    if request.session['role'] == "admin":
        queries = "delete from toys_rent.item i where i.nama='" + name + "';"
        with connection.cursor() as cursor:
            cursor.execute(queries)
        connection.commit()
        return HttpResponseRedirect('/item/')

    else:
        return HttpResponseRedirect('/')

def additem(request):
    if request.session['role'] == "admin":

        if request.method == 'POST':
            form = AddItemForm(request.POST)
            if form.is_valid():
                nama = form.cleaned_data['nama']
                deskripsi = form.cleaned_data['deskripsi']
                usia_minimal = form.cleaned_data['usia_minimal']
                usia_maksimal = form.cleaned_data['usia_maksimal']
                bahan = form.cleaned_data['bahan']
                kategori = form.cleaned_data['kategori']

                # INSERT INTO ITEM
                queries = "insert into toys_rent.item values ('" + nama + "','" + deskripsi + "','" + usia_minimal + "','" + usia_maksimal + "','" + bahan + "');"

                with connection.cursor() as cursor:
                    cursor.execute(queries)
                connection.commit()

                # INSERT INTO KATEGORI_ITEM
                queries = "insert into toys_rent.kategori_item values ('" + nama +  "','" + kategori + "');"

                with connection.cursor() as cursor:
                    cursor.execute(queries)
                connection.commit()

                return HttpResponseRedirect('/item/')

            else:
                form = AddItemForm()
                response['form'] = form

        else:
            form = AddItemForm()
            response['form'] = form
        
        return render(request, 'additem.html', response)

    else:
        return HttpResponseRedirect('/')
        
def updateitem(request, name=None):
    if request.method == 'POST':
        if request.session['role'] == "admin":

            form = AddItemForm(request.POST)
            if form.is_valid():
                nama = form.cleaned_data['nama']
                deskripsi = form.cleaned_data['deskripsi']
                usia_minimal = form.cleaned_data['usia_minimal']
                usia_maksimal = form.cleaned_data['usia_maksimal']
                bahan = form.cleaned_data['bahan']
                kategori = form.cleaned_data['kategori']

                # UPDATE ITEM
                queries = "update toys_rent.item set deskripsi='" + deskripsi + "', usia_dari='" + usia_minimal + "', usia_sampai='" + usia_maksimal + "', bahan='" + bahan + "' where nama='" + nama + "';"

                with connection.cursor() as cursor:
                    cursor.execute(queries)
                connection.commit()

                # UPDATE KATEGORI_ITEM
                queries = "update toys_rent.kategori_item set nama_kategori='" + kategori + "' where nama_item='" + nama + "';"
                with connection.cursor() as cursor:
                    cursor.execute(queries)
                connection.commit()

                return HttpResponseRedirect('/item/')

        else:
            return HttpResponseRedirect('/')

    else:
        if request.session['role'] == "admin":
            # PREPOPULATE FORM
            # QUERY ITEM
            queries = "select * from toys_rent.item i where i.nama='" + name + "';"

            rows = []
            with connection.cursor() as cursor:
                cursor.execute(queries)
                rows = cursor.fetchall()[0]

            nama = rows[0]
            deskripsi = rows[1]
            usia_dari = rows[2]
            usia_sampai = rows[3]
            bahan = rows[4]

            # QUERY KATEGORI_ITEM
            queries = "select * from toys_rent.kategori_item i where i.nama_item='" + name + "';"
            with connection.cursor() as cursor:
                cursor.execute(queries)
                rows = cursor.fetchall()[0]

            kategori = rows[1]

            form = AddItemForm({'nama' : nama, 'deskripsi' : deskripsi, 'usia_minimal' : usia_dari, 'usia_maksimal' : usia_sampai, 'bahan' : bahan, 'kategori' : kategori})

            response['form'] = form

            return render(request, 'updateitem.html', response)
        else:
            return HttpResponseRedirect('/')

def displaybarang(request, item_name):
    response['item_name'] = item_name

    # QUERY BARANG
    queries = "select * from toys_rent.barang b where b.nama_item='" + item_name + "';"

    rows = []
    with connection.cursor() as cursor:
        cursor.execute(queries)
        rows = cursor.fetchall()

    response['barang'] = []
    for row in rows:
        response['barang'].append(row[0])

    return render(request, 'displaybarang.html', response)

