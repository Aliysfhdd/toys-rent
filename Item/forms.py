from django import forms
from django.db import connection

class AddItemForm(forms.Form):
	nama = forms.CharField(label="Nama Item", max_length=30, widget=forms.TextInput(attrs={'placeholder' : 'Masukkan nama item', 'class' : 'form-control'}), required=True)
	deskripsi = forms.CharField(label="Deskripsi Item", max_length=300, widget=forms.Textarea(attrs={'placeholder' : 'Masukkan deskripsi item', 'class' : 'form-control'}))
	usia_minimal = forms.CharField(label="Usia Minimal", max_length=30, widget=forms.TextInput(attrs={'placeholder' : 'Masukkan usia minimal item (dalam bulan)', 'class' : 'form-control'}), required=True)
	usia_maksimal = forms.CharField(label="Usia Maksimal", max_length=30, widget=forms.TextInput(attrs={'placeholder' : 'Masukkan usia maksimal item (dalam bulan)', 'class' : 'form-control'}), required=True)
	bahan = forms.CharField(label="Bahan", max_length=30, widget=forms.TextInput(attrs={'placeholder' : 'Masukkan bahan item', 'class' : 'form-control'}))

	# AMBIL DATA KATEGORI, JADIIN DROPDOWN
	queries = "select nama from toys_rent.kategori;"
	with connection.cursor() as cursor:
		cursor.execute(queries)
		rows = cursor.fetchall()

	kategori = []
	for row in rows:
		kategori.append(row[0])
	
	CATEGORY_CHOICES = []
	for row in kategori:
		CATEGORY_CHOICES.append((row, row.title().replace("_", " ")))

	kategori = forms.CharField(label='Kategori', widget=forms.Select(attrs={'class' : 'form-control'}, choices = CATEGORY_CHOICES))