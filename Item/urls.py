from django.urls import path
from .views import displayitems, additem, updateitem, deleteitem, displaybarang

app_name= "Item"
urlpatterns = [
    path('', displayitems, name='displayitems'),
    path('filter/<category>/', displayitems, name='filterbycategory'),
    path('sort/<sortby>/', displayitems, name='sortby'),
    path('add/', additem, name='additem'),
    path('update/', updateitem, name='update'),
    path('update/<name>/', updateitem, name='updateitem'),
    path('delete/<name>/', deleteitem, name='deleteitem'),
    path('displaybarang/<item_name>/', displaybarang, name='displaybarang')
]
