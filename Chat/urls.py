from django.urls import path
from .views import chat,chatUser, chatbox,send, sendUser

app_name= 'Chat'
urlpatterns = [
    path('admin/', chat, name='admin'),
    path('', chatUser, name='chat'),
    path('<ktp>', chatbox, name='box'),
    path('send/<ktp>', send, name='send'),
    path('sendUser/', sendUser, name='sendUser')
    
]
