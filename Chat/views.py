from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
import string, random, datetime
from django.db import  connection

def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))



response={}
def chatUser(request):
    if request.session['role']=='anggota':
        ktp= "'"+ request.session['no_ktp']+ "'"
        create_table_query = '''Select distinct pesan, date_time, nama_lengkap, no_ktp_anggota from toys_rent.chat c, toys_rent.pengguna p where no_ktp_anggota='''+ ktp +''' and p.no_ktp=no_ktp_anggota  order by date_time; '''
        with connection.cursor() as cursor:
            cursor.execute(create_table_query)
            rows = cursor.fetchall()
        list_chat=[]
        for i in rows:
            list_chat.append([i[0], i[1], i[2], i[3]])
        response['chat']=list_chat
        return render(request, 'chatUser.html', response)   
    elif request.session['role']=='admin':
        return redirect('Chat:admin')
    else:
        return redirect('Welcome:welcome')
        
def chat(request):
    if request.session['role']=='anggota':
        return redirect('Chat:chat') 
    elif request.session['role']=='admin':
        create_table_query = '''Select distinct nama_lengkap, p.no_ktp from toys_rent.pengguna p, toys_rent.chat c where c.no_ktp_anggota=p.no_ktp; '''
        with connection.cursor() as cursor:
            cursor.execute(create_table_query)
            rows = cursor.fetchall()
        list_pengguna=[]
        for i in rows:
            list_pengguna.append([i[0], i[1]])
        response['list']=list_pengguna
        
        return render(request, 'chat.html', response)
       
    else:
        return redirect('Welcome:welcome')

def chatbox(request, ktp=None):
 
    response['ktp']=ktp
    ktp="'"+ ktp  + "'"
    ktp_admin= "'"+ request.session['no_ktp']+ "'"
    print(ktp_admin)
    if request.session['role']=='anggota':
        return redirect('Chat:chat') 
    elif request.session['role']=='admin':
        create_table_query = '''Select distinct pesan, date_time, nama_lengkap, no_ktp_anggota from toys_rent.chat c, toys_rent.pengguna p where no_ktp_anggota='''+ ktp +''' and p.no_ktp=no_ktp_anggota and no_ktp_admin='''+ktp_admin +'''  order by date_time; '''
        with connection.cursor() as cursor:
            cursor.execute(create_table_query)
            rows = cursor.fetchall()
        list_chat=[]
        for i in rows:
            list_chat.append([i[0], i[1], i[2], i[3]])
        response['chat']=list_chat
        
        return render(request, 'chatbox.html', response)
    else:
        return redirect('Welcome:welcome')

def send(request, ktp=None):
    isi_pesan= "'"+request.POST['isi']+"'"
    id="'" + id_generator() +"'"
    ktp_anggota="'"+ktp+"'"
    ktp_admin= "'"+ request.session['no_ktp']+ "'"
    date_time= datetime.datetime.now()
    date_time=date_time.strftime('%Y-%m-%d %H:%M:%S')
    date_time= "'"+date_time+"'"
    query = '''insert into toys_rent.chat values ('''+id+''','''+isi_pesan+''','''+date_time+''','''+ktp_anggota+''','''+ktp_admin+'''); '''
    with connection.cursor() as cursor:
        cursor.execute(query)
    connection.commit()
    return redirect("/chat/"+ktp)

def sendUser(request):
    isi_pesan= "'"+request.POST['isi']+"'"
    id="'" + id_generator() +"'"
    ktp_admin="'1234567894'"
    ktp_anggota= "'"+ request.session['no_ktp']+ "'"
    date_time= datetime.datetime.now()
    date_time=date_time.strftime('%Y-%m-%d %H:%M:%S')
    date_time= "'"+date_time+"'"
    query = '''insert into toys_rent.chat values ('''+id+''','''+isi_pesan+''','''+date_time+''','''+ktp_anggota+''','''+ktp_admin+'''); '''
    with connection.cursor() as cursor:
        cursor.execute(query)
    connection.commit()
    return redirect('Chat:chat')
